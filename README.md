Node_exporter ansible role
=========

Installs prometheus node_exporter

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install node_exporter
  hosts: app_vms
  become: true
  gather_facts: true
  roles:
    - node_exporter
      tags:
        - role_node_exporter
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
